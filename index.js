const express = require('express');
const tf = require('@tensorflow/tfjs');
const tfn = require("@tensorflow/tfjs-node");
const fs = require("fs");

const app = express();

function predict(model, filePath) {
  fs.readFile(filePath, async (err, data) => {
    if(err) {
      console.log("Error reading file");
      return;
    }

    image = tfn.node.decodeImage(data).div(255);
    image = tf.image.resizeBilinear(image, size=[250, 250]);
    image = image.reshape([-1,250,250,3]);

    result = model.predict(image).dataSync();
    console.log(result[0]);

  });
}

app.get('/', async (req, res) => {
    const handler = tfn.io.fileSystem("./tfjs_model/model.json");
    const model = await tf.loadLayersModel(handler);
    
    predict(model, "./real-world/gsd/gsd.jpg");
    predict(model, "./real-world/gsd/gsd-1.jpg");
    predict(model, "./real-world/golden/golden.jpg");
    predict(model, "./real-world/golden/retriever-1.jpg");

    return res.send('Hello world');
});

app.listen(3000, () =>
  console.log(`Listening on port 3000!`),
);